(function() {
    	// Set options for the button
	var UIItemProperties = {
		disabled: false,
		title: 'はてな',
		icon: 'images/icon_18.png',
        popup: {
          href: 'http://b.hatena.ne.jp/touch',
          width: 320,
          height: 480
        }
    };
    
    // Create the button
    var button = opera.contexts.toolbar.createItem( UIItemProperties ); 
    
    // Add the button to the UI
    opera.contexts.toolbar.addItem(button);
})();

(function() {
    	// Set options for the button
	var UIItemProperties = {
		disabled: false,
		title: 'アマゾンウィッシュリスト',
		icon: 'images/icon_18.png'
    };
    
    // Create the button
    var button = opera.contexts.toolbar.createItem( UIItemProperties ); 
    
    button.onclick = function() {
        opera.extension.broadcastMessage('wishlist');
        opera.postError('Hello');
    };
    
    // Add the button to the UI
    opera.contexts.toolbar.addItem(button);
    
})();

/*
(function() {
	// Set options for the button
	var UIItemProperties = {
		disabled: false,
		title: 'Amazon Wish List',
		icon: 'images/icon_18.png'
    };
    
    // Create the button
    var button = opera.contexts.toolbar.createItem( UIItemProperties ); 
    
    // Define what happens when the button is clicked
    button.onclick = function() {
        opera.extension.broadcastMessage('go');
    };
    
    // Add the button to the UI
    opera.contexts.toolbar.addItem(button);
})();
*/

// DOMの用意ができたら、関数を実行する
window.addEventListener('DOMContentLoaded', function() {
    
    // まず、背景色をピンクにする
    document.body.style.background = 'pink';
    
    // そして、ページの全てのリンク（<a>要素）を変数（配列）にする
    var links = document.querySelectorAll('a');
    
    // 最後に、各リンクの文字色をマゼンタにする。ぎゃあああ、かわいいいい！
    for (var i = 0, len = links.length; i < len; i++) {
        links[i].style.color = 'magenta';
    }
    
}, false);
